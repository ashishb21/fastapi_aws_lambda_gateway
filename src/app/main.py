

from fastapi import FastAPI

# from app.api.routes.router import api_router
# from app.core.config import (API_PREFIX, APP_NAME, APP_VERSION,
#                              IS_DEBUG)
# from app.core.event_handlers import (start_app_handler,
#                                      stop_app_handler)
from app.api.routes import home
from mangum import Mangum



'''
Mangum is an adapter for using ASGI applications with AWS Lambda & API Gateway. 
It is intended to provide an easy-to-use, configurable wrapper for any ASGI application deployed in an AWS Lambda function to handle API Gateway requests and responses.
'''

def get_app() -> FastAPI:
    fast_app = FastAPI(title="test", version=1.1)
    # fast_app.include_router(api_router, prefix=API_PREFIX)
    fast_app.include_router(home.router)
    # fast_app.add_event_handler("startup", start_app_handler(fast_app))
    # fast_app.add_event_handler("shutdown", stop_app_handler(fast_app))

    return fast_app


app = get_app()
handler=Mangum(app)
