from fastapi import APIRouter

router = APIRouter()

@router.get("/")
async def get_home() :
    return {"Status":"Working"}

@router.get("/version")
async def get_version()-> dict:
    return {"version": 1.1}

@router.get("/version11")
async def get_version()-> dict:
    return {"version": 1.2}

@router.get("/version12")
async def get_version()-> dict:
    return {"version": 1.3}